import { usFederalDistrict } from './locations/federal-district';
import { usFederalState } from './locations/federal-state';
import { usArmedForces } from './locations/us-armed-forces';
import { usStates } from './locations/us-states';
import { usTerritories } from './locations/us-territories';
import type { IUSLocation } from './model';

export const usLocations: Array<IUSLocation> = [
  ...usFederalState,
  ...usFederalDistrict,
  ...usStates,
  ...usTerritories,
  ...usArmedForces,
];

export {
  usFederalDistrict,
  usFederalState,
  usArmedForces,
  usStates,
  usTerritories,
};
export type { IUSLocation };
