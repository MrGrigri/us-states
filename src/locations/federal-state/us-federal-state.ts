import { IUSLocation } from '../../model';

export const usFederalState: Array<IUSLocation> = [
  {
    name: 'United States of America',
    status: 'federal state',
    capital: 'District of Colombia',
    codes: {
      ansi: 'US 00',
      iso: ['US', 'USA', '840'],
    },
    abbreviations: {
      gpo: 'U.S.',
      ap: 'U.S.',
      other: ['USA'],
    },
  },
];
