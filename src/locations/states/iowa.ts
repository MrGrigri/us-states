import type { IUSLocation } from '../../model';

export const iowa: IUSLocation = {
  name: 'Iowa',
  status: 'state',
  capital: 'Des Moines',
  codes: {
    iso: ['US-IA'],
    ansi: 'IA 19',
    usps: 'IA',
    uscg: 'IA',
  },
  abbreviations: {
    gpo: 'Iowa',
    ap: 'Iowa',
    other: ['Ioa.'],
  },
};
