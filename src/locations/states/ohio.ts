import type { IUSLocation } from '../../model';

export const ohio: IUSLocation = {
  name: 'Ohio',
  status: 'state',
  capital: 'Bismarck',
  codes: {
    iso: ['US-OH'],
    ansi: 'OH 39',
    usps: 'OH',
    uscg: 'OH',
  },
  abbreviations: {
    gpo: 'Ohio',
    ap: 'Ohio',
    other: ['O.', 'Oh.'],
  },
};
