import type { IUSLocation } from '../../model';

export const southDakota: IUSLocation = {
  name: 'South Dakota',
  status: 'state',
  capital: 'Pierre',
  codes: {
    iso: ['US-SD'],
    ansi: 'SD 45',
    usps: 'SD',
    uscg: 'SD',
  },
  abbreviations: {
    gpo: 'S. Dak.',
    ap: 'S.D.',
    other: ['SoDak'],
  },
};
