import type { IUSLocation } from '../../model';

export const arizona: IUSLocation = {
  name: 'Arizona',
  status: 'state',
  capital: 'Phoenix',
  codes: {
    iso: ['US-AZ'],
    ansi: 'AZ 04',
    usps: 'AZ',
    uscg: 'AZ',
  },
  abbreviations: {
    gpo: 'Ariz.',
    ap: 'Ariz.',
  },
};
