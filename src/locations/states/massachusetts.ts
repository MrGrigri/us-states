import type { IUSLocation } from '../../model';

export const massachusetts: IUSLocation = {
  name: 'Massachusetts',
  status: 'state',
  capital: 'Boston',
  codes: {
    iso: ['US-MA'],
    ansi: 'MA 25',
    usps: 'MA',
    uscg: 'MS',
  },
  abbreviations: {
    gpo: 'Mass.',
    ap: 'Mass.',
  },
};
