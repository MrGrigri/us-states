import type { IUSLocation } from '../../model';

export const missouri: IUSLocation = {
  name: 'Missouri',
  status: 'state',
  capital: 'Jefferson City',
  codes: {
    iso: ['US-MO'],
    ansi: 'MO 29',
    usps: 'MO',
    uscg: 'MO',
  },
  abbreviations: {
    gpo: 'Mo.',
    ap: 'Mo.',
  },
};
