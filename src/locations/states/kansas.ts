import type { IUSLocation } from '../../model';

export const kansas: IUSLocation = {
  name: 'Kansas',
  status: 'state',
  capital: 'Topeka',
  codes: {
    iso: ['US-KS'],
    ansi: 'KS 20',
    usps: 'KS',
    uscg: 'KA',
  },
  abbreviations: {
    gpo: 'Kans.',
    ap: 'Kan.',
    other: ['Ka.'],
  },
};
