import type { IUSLocation } from '../../model';

export const maryland: IUSLocation = {
  name: 'Maryland',
  status: 'state',
  capital: 'Annapolis',
  codes: {
    iso: ['US-MD'],
    ansi: 'MD 24',
    usps: 'MD',
    uscg: 'MD',
  },
  abbreviations: {
    gpo: 'Md.',
    ap: 'Md.',
    other: ['Mar.', 'Mary.'],
  },
};
