import type { IUSLocation } from '../../model';

export const alaska: IUSLocation = {
  name: 'Alaska',
  status: 'state',
  capital: 'Juneau',
  codes: {
    iso: ['US-AK'],
    ansi: 'AK 02',
    usps: 'AK',
    uscg: 'AK',
  },
  abbreviations: {
    gpo: 'Alaska',
    ap: 'Alaska',
    other: ['Ak.'],
  },
};
