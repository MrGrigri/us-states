import type { IUSLocation } from '../../model';

export const virginia: IUSLocation = {
  name: 'Virginia',
  status: 'state',
  capital: 'Richmond',
  codes: {
    iso: ['US-VA'],
    ansi: 'VA 51',
    usps: 'VA',
    uscg: 'VA',
  },
  abbreviations: {
    gpo: 'Va.',
    ap: 'Va.',
    other: ['Virg.'],
  },
};
