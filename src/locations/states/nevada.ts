import type { IUSLocation } from '../../model';

export const nevada: IUSLocation = {
  name: 'Nevada',
  status: 'state',
  capital: 'Carson City',
  codes: {
    iso: ['US-NV'],
    ansi: 'NV 32',
    usps: 'NV',
    uscg: 'NV',
  },
  abbreviations: {
    gpo: 'Nev.',
    ap: 'Nev.',
  },
};
