import type { IUSLocation } from '../../model';

export const illinois: IUSLocation = {
  name: 'Illinois',
  status: 'state',
  capital: 'Springfield',
  codes: {
    iso: ['US-IL'],
    ansi: 'IL 17',
    usps: 'IL',
    uscg: 'IL',
  },
  abbreviations: {
    gpo: 'Ill.',
    ap: 'Ill.',
  },
};
