import type { IUSLocation } from '../../model';

export const newHampshire: IUSLocation = {
  name: 'New Hampshire',
  status: 'state',
  capital: 'Concord',
  codes: {
    iso: ['US-NH'],
    ansi: 'NH 33',
    usps: 'NH',
    uscg: 'NH',
  },
  abbreviations: {
    gpo: 'N.H.',
    ap: 'N.H.',
  },
};
