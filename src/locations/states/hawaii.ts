import type { IUSLocation } from '../../model';

export const hawaii: IUSLocation = {
  name: 'Hawaii',
  status: 'state',
  capital: 'Honolulu',
  codes: {
    iso: ['US-HI'],
    ansi: 'HI 15',
    usps: 'HI',
    uscg: 'HA',
  },
  abbreviations: {
    gpo: 'Hawaii',
    ap: 'Hawaii',
    other: ['Hi.'],
  },
};
