import type { IUSLocation } from '../../model';

export const newMexico: IUSLocation = {
  name: 'New Mexico',
  status: 'state',
  capital: 'Santa Fe',
  codes: {
    iso: ['US-NM'],
    ansi: 'NM 35',
    usps: 'NM',
    uscg: 'NM',
  },
  abbreviations: {
    gpo: 'N. Mex.',
    ap: 'N.M.',
    other: ['New M.', 'New Mex.'],
  },
};
