import type { IUSLocation } from '../../model';

export const arkansas: IUSLocation = {
  name: 'Arkansas',
  status: 'state',
  capital: 'Little Rock',
  codes: {
    iso: ['US-AR'],
    ansi: 'AR 05',
    usps: 'AR',
    uscg: 'AR',
  },
  abbreviations: {
    gpo: 'Ark.',
    ap: 'Ark.',
  },
};
