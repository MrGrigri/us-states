import type { IUSLocation } from '../../model';

export const delaware: IUSLocation = {
  name: 'Delaware',
  status: 'state',
  capital: 'Dover',
  codes: {
    iso: ['US-DE'],
    ansi: 'DE 10',
    usps: 'DE',
    uscg: 'DL',
  },
  abbreviations: {
    gpo: 'Del.',
    ap: 'Del.',
  },
};
