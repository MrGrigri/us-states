import type { IUSLocation } from '../../model';

export const kentucky: IUSLocation = {
  name: 'Kentucky',
  status: 'state',
  capital: 'Frankfort',
  codes: {
    iso: ['US-KY'],
    ansi: 'KY 21',
    usps: 'KY',
    uscg: 'KY',
  },
  abbreviations: {
    gpo: 'ky.',
    ap: 'ky.',
    other: ['Ken.', 'Kent.'],
  },
};
