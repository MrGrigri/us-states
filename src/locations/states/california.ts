import type { IUSLocation } from '../../model';

export const california: IUSLocation = {
  name: 'California',
  status: 'state',
  capital: 'Sacramento',
  codes: {
    iso: ['US-CA'],
    ansi: 'CA 06',
    usps: 'CA',
    uscg: 'CF',
  },
  abbreviations: {
    gpo: 'Calf.',
    ap: 'Calf.',
    other: ['Cal.'],
  },
};
