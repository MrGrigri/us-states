import type { IUSLocation } from '../../model';

export const connecticut: IUSLocation = {
  name: 'Connecticut',
  status: 'state',
  capital: 'Hartford',
  codes: {
    iso: ['US-CT'],
    ansi: 'CT 09',
    usps: 'CT',
    uscg: 'CT',
  },
  abbreviations: {
    gpo: 'Conn.',
    ap: 'Conn.',
  },
};
