import type { IUSLocation } from '../../model';

export const oklahoma: IUSLocation = {
  name: 'Oklahoma',
  status: 'state',
  capital: 'Oklahoma City',
  codes: {
    iso: ['US-OK'],
    ansi: 'OK 40',
    usps: 'OK',
    uscg: 'OK',
  },
  abbreviations: {
    gpo: 'Okla.',
    ap: 'Okla.',
  },
};
