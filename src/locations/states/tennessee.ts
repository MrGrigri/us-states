import type { IUSLocation } from '../../model';

export const tennessee: IUSLocation = {
  name: 'Tennessee',
  status: 'state',
  capital: 'Nashville',
  codes: {
    iso: ['US-TN'],
    ansi: 'TN 47',
    usps: 'TN',
    uscg: 'TN',
  },
  abbreviations: {
    gpo: 'Tenn.',
    ap: 'Tenn.',
  },
};
