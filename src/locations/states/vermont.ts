import type { IUSLocation } from '../../model';

export const vermont: IUSLocation = {
  name: 'Vermont',
  status: 'state',
  capital: 'Montpelier',
  codes: {
    iso: ['US-VT'],
    ansi: 'VT 50',
    usps: 'VT',
    uscg: 'VT',
  },
  abbreviations: {
    gpo: 'Vt.',
    ap: 'Vt.',
    other: ['Verm.'],
  },
};
