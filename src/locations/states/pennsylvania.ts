import type { IUSLocation } from '../../model';

export const pennsylvania: IUSLocation = {
  name: 'Pennsylvania',
  status: 'state',
  capital: 'Harrisburg',
  codes: {
    iso: ['US-PA'],
    ansi: 'PA 42',
    usps: 'PA',
    uscg: 'PA',
  },
  abbreviations: {
    gpo: 'Pa.',
    ap: 'Pa.',
    other: ['Penn.', 'Penna.'],
  },
};
