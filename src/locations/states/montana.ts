import type { IUSLocation } from '../../model';

export const montana: IUSLocation = {
  name: 'Montana',
  status: 'state',
  capital: 'Helena',
  codes: {
    iso: ['US-MT'],
    ansi: 'MT 30',
    usps: 'MT',
    uscg: 'MT',
  },
  abbreviations: {
    gpo: 'Mont.',
    ap: 'Mont.',
  },
};
