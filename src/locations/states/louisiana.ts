import type { IUSLocation } from '../../model';

export const louisiana: IUSLocation = {
  name: 'Louisiana',
  status: 'state',
  capital: 'Baton Rouge',
  codes: {
    iso: ['US-LA'],
    ansi: 'LA 22',
    usps: 'LA',
    uscg: 'LA',
  },
  abbreviations: {
    gpo: 'La.',
    ap: 'La.',
  },
};
