import type { IUSLocation } from '../../model';

export const oregon: IUSLocation = {
  name: 'Oregon',
  status: 'state',
  capital: 'Salem',
  codes: {
    iso: ['US-OR'],
    ansi: 'OR 41',
    usps: 'OR',
    uscg: 'OR',
  },
  abbreviations: {
    gpo: 'Oreg.',
    ap: 'Ore.',
  },
};
