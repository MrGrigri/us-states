import type { IUSLocation } from '../../model';

export const utah: IUSLocation = {
  name: 'Utah',
  status: 'state',
  capital: 'Salt Lake City',
  codes: {
    iso: ['US-UT'],
    ansi: 'UT 49',
    usps: 'UT',
    uscg: 'UT',
  },
  abbreviations: {
    gpo: 'Utah',
    ap: 'Utah',
    other: ['Ut.'],
  },
};
