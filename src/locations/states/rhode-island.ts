import type { IUSLocation } from '../../model';

export const rhodeIsland: IUSLocation = {
  name: 'Rhode Island',
  status: 'state',
  capital: 'Providence',
  codes: {
    iso: ['US-RI'],
    ansi: 'RI 44',
    usps: 'RI',
    uscg: 'RI',
  },
  abbreviations: {
    gpo: 'R.I.',
    ap: 'R.I.',
    other: ['P. P.'],
  },
};
