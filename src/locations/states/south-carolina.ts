import type { IUSLocation } from '../../model';

export const southCarolina: IUSLocation = {
  name: 'South Carolina',
  status: 'state',
  capital: 'Columbia',
  codes: {
    iso: ['US-SC'],
    ansi: 'SC 46',
    usps: 'SC',
    uscg: 'SC',
  },
  abbreviations: {
    gpo: 'S.C.',
    ap: 'S.C.',
    other: ['S. Car.'],
  },
};
