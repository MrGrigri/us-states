import type { IUSLocation } from '../../model';

export const northCarolina: IUSLocation = {
  name: 'North Carolina',
  status: 'state',
  capital: 'Raleigh',
  codes: {
    iso: ['US-NC'],
    ansi: 'NC 37',
    usps: 'NC',
    uscg: 'NC',
  },
  abbreviations: {
    gpo: 'N.C.',
    ap: 'N.C.',
    other: ['N. Car.'],
  },
};
