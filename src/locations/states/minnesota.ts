import type { IUSLocation } from '../../model';

export const minnesota: IUSLocation = {
  name: 'Minnesota',
  status: 'state',
  capital: 'Saint Paul',
  codes: {
    iso: ['US-MN'],
    ansi: 'MN 27',
    usps: 'MN',
    uscg: 'MN',
  },
  abbreviations: {
    gpo: 'Minn.',
    ap: 'Minn.',
  },
};
