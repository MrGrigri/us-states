import type { IUSLocation } from '../../model';

export const georgia: IUSLocation = {
  name: 'Georgia',
  status: 'state',
  capital: 'Atlanta',
  codes: {
    iso: ['US-GA'],
    ansi: 'GA 13',
    usps: 'GA',
    uscg: 'GA',
  },
  abbreviations: {
    gpo: 'Ga.',
    ap: 'Ga.',
    other: ['Geo.'],
  },
};
