import type { IUSLocation } from '../../model';

export const idaho: IUSLocation = {
  name: 'Idaho',
  status: 'state',
  capital: 'Boise',
  codes: {
    iso: ['US-ID'],
    ansi: 'ID 16',
    usps: 'ID',
    uscg: 'ID',
  },
  abbreviations: {
    gpo: 'Idaho',
    ap: 'Idaho',
    other: ['Ida.'],
  },
};
