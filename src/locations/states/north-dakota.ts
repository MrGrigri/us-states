import type { IUSLocation } from '../../model';

export const northDakota: IUSLocation = {
  name: 'North Dakota',
  status: 'state',
  capital: 'Bismarck',
  codes: {
    iso: ['US-ND'],
    ansi: 'ND 38',
    usps: 'ND',
    uscg: 'ND',
  },
  abbreviations: {
    gpo: 'N. Dak.',
    ap: 'N.D.',
  },
};
