import type { IUSLocation } from '../../model';

export const wyoming: IUSLocation = {
  name: 'Wyoming',
  status: 'state',
  capital: 'Cheyenne',
  codes: {
    iso: ['US-WY'],
    ansi: 'WY 56',
    usps: 'WY',
    uscg: 'WY',
  },
  abbreviations: {
    gpo: 'Wyo.',
    ap: 'Wyo.',
  },
};
