import type { IUSLocation } from '../../model';

export const westVirginia: IUSLocation = {
  name: 'West Virginia',
  status: 'state',
  capital: 'Charleston',
  codes: {
    iso: ['US-WV'],
    ansi: 'WV 54',
    usps: 'WV',
    uscg: 'WV',
  },
  abbreviations: {
    gpo: 'W. Va.',
    ap: 'W.Va.',
    other: ['W.V.', 'W. Virg.'],
  },
};
