import type { IUSLocation } from '../../model';

export const colorado: IUSLocation = {
  name: 'Colorado',
  status: 'state',
  capital: 'Denver',
  codes: {
    iso: ['US-CO'],
    ansi: 'CO 08',
    usps: 'CO',
    uscg: 'CL',
  },
  abbreviations: {
    gpo: 'Colo.',
    ap: 'Colo.',
  },
};
