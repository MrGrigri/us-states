import type { IUSLocation } from '../../model';

export const florida: IUSLocation = {
  name: 'Florida',
  status: 'state',
  capital: 'Tallahassee',
  codes: {
    iso: ['US-FL'],
    ansi: 'FL 12',
    usps: 'FL',
    uscg: 'FL',
  },
  abbreviations: {
    gpo: 'Fla.',
    ap: 'Fla.',
  },
};
