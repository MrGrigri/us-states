import type { IUSLocation } from '../../model';

export const wisconsin: IUSLocation = {
  name: 'Wisconsin',
  status: 'state',
  capital: 'Madison',
  codes: {
    iso: ['US-WI'],
    ansi: 'WI 55',
    usps: 'WI',
    uscg: 'WS',
  },
  abbreviations: {
    gpo: 'Wis.',
    ap: 'Wis.',
    other: ['Wisc.'],
  },
};
