import type { IUSLocation } from '../../model';

export const mississippi: IUSLocation = {
  name: 'Mississippi',
  status: 'state',
  capital: 'Jackson',
  codes: {
    iso: ['US-MS'],
    ansi: 'MS 28',
    usps: 'MS',
    uscg: 'MI',
  },
  abbreviations: {
    gpo: 'Miss.',
    ap: 'Miss.',
  },
};
