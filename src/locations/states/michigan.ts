import type { IUSLocation } from '../../model';

export const michigan: IUSLocation = {
  name: 'Michigan',
  status: 'state',
  capital: 'Lansing',
  codes: {
    iso: ['US-MI'],
    ansi: 'MI 26',
    usps: 'MI',
    uscg: 'MC',
  },
  abbreviations: {
    gpo: 'Mich.',
    ap: 'Mich.',
  },
};
