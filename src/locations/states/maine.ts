import type { IUSLocation } from '../../model';

export const maine: IUSLocation = {
  name: 'Maine',
  status: 'state',
  capital: 'Augusta',
  codes: {
    iso: ['US-ME'],
    ansi: 'ME 23',
    usps: 'ME',
    uscg: 'ME',
  },
  abbreviations: {
    gpo: 'Maine',
    ap: 'Maine',
  },
};
