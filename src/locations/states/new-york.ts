import type { IUSLocation } from '../../model';

export const newYork: IUSLocation = {
  name: 'New York',
  status: 'state',
  capital: 'Albany',
  codes: {
    iso: ['US-NY'],
    ansi: 'NY 36',
    usps: 'NY',
    uscg: 'NY',
  },
  abbreviations: {
    gpo: 'N.Y.',
    ap: 'N.Y.',
    other: ['N. York'],
  },
};
