import type { IUSLocation } from '../../model';

export const newJersey: IUSLocation = {
  name: 'New Jersey',
  status: 'state',
  capital: 'Trenton',
  codes: {
    iso: ['US-NJ'],
    ansi: 'NJ 34',
    usps: 'NJ',
    uscg: 'NJ',
  },
  abbreviations: {
    gpo: 'N.J.',
    ap: 'N.J.',
    other: ['N. Jersey'],
  },
};
