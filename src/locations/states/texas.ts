import type { IUSLocation } from '../../model';

export const texas: IUSLocation = {
  name: 'Texas',
  status: 'state',
  capital: 'Austin',
  codes: {
    iso: ['US-TX'],
    ansi: 'TX 48',
    usps: 'TX',
    uscg: 'TX',
  },
  abbreviations: {
    gpo: 'Tex.',
    ap: 'Texas',
  },
};
