import type { IUSLocation } from '../../model';

export const nebraska: IUSLocation = {
  name: 'Nebraska',
  status: 'state',
  capital: 'Lincoln',
  codes: {
    iso: ['US-NE'],
    ansi: 'NE 31',
    usps: 'NE',
    uscg: 'NB',
  },
  abbreviations: {
    gpo: 'Nebr.',
    ap: 'Neb.',
  },
};
