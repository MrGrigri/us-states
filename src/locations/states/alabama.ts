import type { IUSLocation } from '../../model';

export const alabama: IUSLocation = {
  name: 'Alabama',
  status: 'state',
  capital: 'Montgomery',
  codes: {
    iso: ['US-AL'],
    ansi: 'AL 01',
    usps: 'AL',
    uscg: 'AL',
  },
  abbreviations: {
    gpo: 'Ala.',
    ap: 'Ala.',
  },
};
