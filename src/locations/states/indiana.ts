import type { IUSLocation } from '../../model';

export const indiana: IUSLocation = {
  name: 'Indiana',
  status: 'state',
  capital: 'Indianapolis',
  codes: {
    iso: ['US-IN'],
    ansi: 'IN 18',
    usps: 'IN',
    uscg: 'IN',
  },
  abbreviations: {
    gpo: 'Ind.',
    ap: 'Ind.',
  },
};
