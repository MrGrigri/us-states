import type { IUSLocation } from '../../model';

export const washington: IUSLocation = {
  name: 'Washington',
  status: 'state',
  capital: 'Olympia',
  codes: {
    iso: ['US-WA'],
    ansi: 'WA 53',
    usps: 'WA',
    uscg: 'WN',
  },
  abbreviations: {
    gpo: 'Wash.',
    ap: 'Wash.',
    other: ['Wn.'],
  },
};
