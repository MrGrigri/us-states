import type { IUSLocation } from '../../model';

export const americanSamoa: IUSLocation = {
  name: 'American Samoa',
  status: 'insular area territory',
  capital: 'Pago Pago',
  codes: {
    iso: ['AS', 'ASM', '016', 'US-AS'],
    ansi: 'AS 60',
    usps: 'AS',
    uscg: 'AS',
  },
  abbreviations: {
    gpo: 'A.S.',
  },
};
