import type { IUSLocation } from '../../model';

export const northernMarianaIslands: IUSLocation = {
  name: 'Northern Mariana Islands',
  status: 'insular area commonwealth',
  capital: 'Saipan',
  codes: {
    iso: ['MP', 'MNP', '580', 'US-MP'],
    ansi: 'MP 69',
    usps: 'MP',
    uscg: 'CM',
  },
  abbreviations: {
    gpo: 'M.P.',
    other: ['CNMI'],
  },
};
