import type { IUSLocation } from '../../model';

export const puertoRico: IUSLocation = {
  name: 'Puerto Rico',
  status: 'insular area commonwealth',
  capital: 'San Juan',
  codes: {
    iso: ['PR', 'PRI', '630', 'US-PR'],
    ansi: 'PR 72',
    usps: 'PR',
    uscg: 'PR',
  },
  abbreviations: {
    gpo: 'P.R.',
  },
};
