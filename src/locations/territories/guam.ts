import type { IUSLocation } from '../../model';

export const guam: IUSLocation = {
  name: 'Guam',
  status: 'insular area territory',
  capital: 'Hagåtña',
  codes: {
    iso: ['GU', 'GUM', '316', 'US-GU'],
    ansi: 'GU 66',
    usps: 'GU',
    uscg: 'GU',
  },
  abbreviations: {
    gpo: 'Guam',
  },
};
