import type { IUSLocation } from '../../model';

export const unitedStatesVirginIslands: IUSLocation = {
  name: 'United States Virgin Islands',
  status: 'insular area territory',
  capital: 'Charlotte Amalie',
  codes: {
    iso: ['VI', 'VIR', '850', 'US-VI'],
    ansi: 'VI 78',
    usps: 'VI',
    uscg: 'VI',
  },
  abbreviations: {
    gpo: 'V.I.',
    other: ['U.S.V.I'],
  },
};
