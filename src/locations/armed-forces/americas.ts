import type { IUSLocation } from '../../model';

export const americas: IUSLocation = {
  name: 'U.S. Armed Forces -- Americas',
  status: 'us military mail code',
  codes: {
    usps: 'AA',
  },
};
