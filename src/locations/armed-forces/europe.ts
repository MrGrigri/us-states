import type { IUSLocation } from '../../model';

export const europe: IUSLocation = {
  name: 'U.S. Armed Forces -- Europe',
  status: 'us military mail code',
  codes: {
    usps: 'AE',
  },
};
