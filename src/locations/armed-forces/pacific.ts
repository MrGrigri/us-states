import type { IUSLocation } from '../../model';

export const pacific: IUSLocation = {
  name: 'U.S. Armed Forces -- Pacific',
  status: 'us military mail code',
  codes: {
    usps: 'AP',
  },
};
