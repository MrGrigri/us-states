import type { TUSLocations } from '../model';
import * as us_armed_forces from './armed-forces';

export const usArmedForces: TUSLocations = [
  us_armed_forces.americas,
  us_armed_forces.europe,
  us_armed_forces.pacific,
];
