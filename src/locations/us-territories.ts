import type { TUSLocations } from '../model';
import * as us_territories from './territories';

export const usTerritories: TUSLocations = [
  us_territories.americanSamoa,
  us_territories.guam,
  us_territories.northernMarianaIslands,
  us_territories.puertoRico,
  us_territories.unitedStatesVirginIslands,
];
