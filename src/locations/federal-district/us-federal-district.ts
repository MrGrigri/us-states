import type { IUSLocation } from '../../model';

export const usFederalDistrict: Array<IUSLocation> = [
  {
    name: 'District of Colombia',
    status: 'federal district',
    codes: {
      iso: ['US-DC'],
      ansi: 'DC 11',
      usps: 'DC',
      uscg: 'DC',
    },
    abbreviations: {
      gpo: 'D.C.',
      ap: 'D.C.',
      other: ['Dis. Col.'],
    },
  },
];
