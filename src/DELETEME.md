<datalist id="us_states">
  <option value="al" label="Alabama">Alabama</option>
  <option value="ak" label="Alaska">Alaska</option>
  <option value="az" label="Arizona">Arizona</option>
  <option value="ar" label="Arkansas">Arkansas</option>
  <option value="ca" label="California">California</option>
  <option value="co" label="Colorado">Colorado</option>
  <option value="ct" label="Connecticut">Connecticut</option>
  <option value="de" label="Delaware">Delaware</option>
  <option value="fl" label="Florida">Florida</option>
  <option value="ga" label="Georgia">Georgia</option>
  <option value="hi" label="Hawaii">Hawaii</option>
  <option value="id" label="Idaho">Idaho</option>
  <option value="il" label="Illinois">Illinois</option>
  <option value="in" label="Indiana">Indiana</option>
  <option value="ia" label="Iowa">Iowa</option>
  <option value="ks" label="Kansas">Kansas</option>
  <option value="ky" label="Kentucky">Kentucky</option>
  <option value="la" label="Louisiana">Louisiana</option>
  <option value="me" label="Maine">Maine</option>
  <option value="md" label="Maryland">Maryland</option>
  <option value="ma" label="Massachusetts">Massachusetts</option>
  <option value="mi" label="Michigan">Michigan</option>
  <option value="mn" label="Minnesota">Minnesota</option>
  <option value="ms" label="Mississippi">Mississippi</option>
  <option value="mo" label="Missouri">Missouri</option>
  <option value="mt" label="Montana">Montana</option>
  <option value="ne" label="Nebraska">Nebraska</option>
  <option value="nv" label="Nevada">Nevada</option>
  <option value="nh" label="New Hampshire">New Hampshire</option>
  <option value="nj" label="New Jersey">New Jersey</option>
  <option value="nm" label="New Mexico">New Mexico</option>
  <option value="ny" label="New York">New York</option>
  <option value="nc" label="North Carolina">North Carolina</option>
  <option value="nd" label="North Dakota">North Dakota</option>
  <option value="oh" label="Ohio">Ohio</option>
  <option value="ok" label="Oklahoma">Oklahoma</option>
  <option value="or" label="Oregon">Oregon</option>
  <option value="pa" label="Pennsylvania">Pennsylvania</option>
  <option value="ri" label="Rhode Island">Rhode Island</option>
  <option value="sc" label="South Carolina">South Carolina</option>
  <option value="ds" label="South Dakota">South Dakota</option>
  <option value="tn" label="Tennessee">Tennessee</option>
  <option value="tx" label="Texas">Texas</option>
  <option value="ut" label="Utah">Utah</option>
  <option value="vt" label="Vermont">Vermont</option>
  <option value="va" label="Virginia">Virginia</option>
  <option value="wa" label="Washington">Washington</option>
  <option value="wv" label="West Virginia">West Virginia</option>
  <option value="wi" label="Wisconsin">Wisconsin</option>
  <option value="wy" label="Wyoming">Wyoming</option>
</datalist>

<datalist id="us_armed_forces">
  <option value="aa" label="Armed Forces Americas">Armed Forces Americas</option>
  <option value="ae" label="Armed Forces Other">Armed Forces Other</option>
  <option value="ap" label="Armed Forces Pacific">Armed Forces Pacific</option>
</datalist>

<datalist id="us_territories">
  <option value="as" label="American Samoa">American Samoa</option>
  <option value="gu" label="Guam">Guam</option>
  <option value="mh" label="Marshall Islands">Marshall Islands</option>
  <option value="pr" label="Puerto Rico">Puerto Rico</option>
  <option value="vi" label="Virgin Islands">Virgin Islands</option>
</datalist>

<datalist id="us_federal_district">
  <option value="dc" label="District of Columbia">District of Columbia</option>
</datalist>

<datalist id="us_freely_associated_states">
  <option value="fm" label="Federated States of Micronesia">Federated States of Micronesia</option>
  <option value="mp" label="Northern Mariana Islands">Northern Mariana Islands</option>
  <option value="pw" label="Palau">Palau</option>
</datalist>