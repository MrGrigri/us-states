export type TStatus =
  | 'state'
  | 'federal state'
  | 'federal district'
  | 'insular area territory'
  | 'insular area commonwealth'
  | 'us military mail code';

export type TUSLocations = Array<IUSLocation>;

export interface ICodes {
  iso?: Array<string>;
  ansi?: string;
  usps?: string;
  uscg?: string;
}

export interface IAbbreviations {
  gpo?: string;
  ap?: string;
  other?: Array<string>;
}

export interface IUSLocation {
  name: string;
  status: TStatus;
  capital?: string;
  codes?: ICodes;
  abbreviations?: IAbbreviations;
}
