# US Location

This is a package that includes all U.S. states (federal state and district), insular areas of territories and commonwealths, and the U.S. military postal codes.

## Installation

```bash
npm i @richkode/us-locations
```

## Usage

The main function is to return all of the aforementioned U.S. locations.

```typescript
import {
  usFederalDistrict,
  usFederalState,
  usArmedForces,
  usStates,
  usTerritories,
} from '@richkode/us-locations';

// Will print out an array of all U.S. locations and metadata associated with each location
console.log(usLocations);

// Will print out an array of U.S. states
console.log(usStates);

// Will print out an array with only one entry of the U.S. (federal state)
console.log(usFederalState);

// Will print out an array with only one entry of the U.S. capital (federal district)
console.log(usFederalDistrict);

// Will print out an array of U.S. territories
console.log(usTerritories);

// Will print out an array of U.S. Armed Forces postal locations
console.log(usArmedForces);
```

## Typings

```typescript
// Location status
type TStatus =
  | 'state'
  | 'federal state'
  | 'federal district'
  | 'insular area territory'
  | 'insular area commonwealth'
  | 'us military mail code';

// Codes Interface
interface ICodes {
  iso?: Array<string>;
  ansi?: string;
  usps?: string;
  uscg?: string;
}

// Abbreviation Interface
interface IAbbreviations {
  gpo?: string;
  ap?: string;
  other?: Array<string>;
}

// Each location has the following metadata
interface IUSLocations {
  name: string;
  status: TStatus;
  capital?: string;
  codes?: ICodes;
  abbreviations?: IAbbreviations;
}

// Example of the state of Rhode Island
/*
{
  name: 'Rhode Island',
  status: 'state',
  capital: 'Providence',
  codes: {
    iso: ['US-RI'],
    ansi: 'RI 44',
    usps: 'RI',
    uscg: 'RI',
  },
  abbreviations: {
    gpo: 'R.I.',
    ap: 'R.I.',
    other: ['P. P.'],
  },
}
 */
```

### codes

#### ISO

[ISO 3166 codes](https://en.wikipedia.org/wiki/ISO_3166) (2-letter, 3-letter, and 3-digit codes from ISO 3166-1; 2+2-letter codes from ISO 3166-2)

#### ANSI

2-letter and 2-digit codes from the ANSI standard [INCITS 38:2009](https://standards.incits.org/apps/group_public/project/details.php?project_id=2399) (supersedes FIPS 5-2)

#### USPS (United States Postal Service)

2-letter codes used by the United States Postal Service

#### USCG (United States Coast Guard)

2-letter codes used by the United States Coast Guard

### Abbreviations

#### GPO (Government Printing Office)

Older variable-length official US Government Printing Office abbreviations

#### AP (Associated Press)

Abbreviations from the AP Stylebook

#### Other

Other official abbreviations used through the federal government.
